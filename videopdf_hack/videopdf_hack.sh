#!/bin/bash
count=0
workspacecount=0
videoarr=(./videos/*)
videoamount=${#videoarr[@]}

readarray -t videopoints < videopoints.txt
if [ ! $videoamount == ${#videopoints[@]} ]; then
	echo "video amount & videopoint amount not matching, exiting"	
	exit 1
fi

for ((i=0; i<$videoamount; i++)); do
	videopoints[i]=$((videopoints[i]+2*i))
    	i3-msg "workspace $((10+i))"
	mpv ${videoarr[$i]} &
	sleep 0.5
done

zathura --page 1 --mode presentation *.pdf &

i3-msg "workspace 1"

xinput test-xi2 --root 3 | gawk '/RawKeyRelease/ {getline; getline; print $2; fflush()}' | while read -r key; do
if [ $key == 114 ]; then
	if [ $count -lt $((42+videoamount*2)) ]; then
		count=$((count+1))
	fi
	echo $count

elif [ $key == 113 ]; then
	if [ $count -gt 0 ]; then
		count=$((count-1))
	fi
	echo $count
fi

if [[ " ${videopoints[*]} " =~ " $((count+1)) " ]]; then
	if [[ " ${videopoints[*]} " =~ " $((lastcount+2)) " ]]; then
		i3-msg "workspace 1"
		xdotool key Shift+j
	elif [[ " ${videopoints[*]} " =~ " $lastcount " ]]; then
		i3-msg "workspace 1"
		xdotool key Shift+k
	fi
elif [[ " ${videopoints[*]} " =~ " $count " ]]; then
	if [[ " ${videopoints[*]} " =~ " $((lastcount+1)) " ]]; then
		xdotool key Shift+j
		i3-msg "workspace $((10+workspacecount))"
	elif [[ " ${videopoints[*]} " =~ " $((lastcount-1)) " ]]; then
		xdotool key p	
	fi
elif [[ " ${videopoints[*]} " =~ " $((count-1)) " ]]; then
	if [[ " ${videopoints[*]} " =~ " $lastcount " ]]; then
		xdotool key p	
	elif [[ " ${videopoints[*]} " =~ " $((lastcount-2)) " ]]; then
		workspacecount=$((workspacecount-1))
		i3-msg "workspace $((10+workspacecount))"
	fi
elif [[ " ${videopoints[*]} " =~ " $((count-2)) " ]]; then
	if [[ " ${videopoints[*]} " =~ " $((lastcount-1)) " ]]; then
		i3-msg "workspace 1"
		workspacecount=$((workspacecount+1))
	elif [[ " ${videopoints[*]} " =~ " $((lastcount-3)) " ]]; then
		i3-msg "workspace 1"
		xdotool key Shift+k
	fi
else
	if [ $key == 114 ]; then
		xdotool key Shift+j
	elif [ $key == 113 ]; then
		xdotool key Shift+k
	fi
fi
lastcount=$count
done
