#!/bin/bash
while [ 1 ]
do
	xdotool key f
	sleep 0.2
	xdotool key $1
	sleep 0.4
	xdotool key Enter
	sleep 0.6
	xdotool key minus
	sleep 1.5
done
