#!/bin/bash
xinput test-xi2 --root 3 | gawk '/RawKeyRelease/ {getline; getline; print $2; fflush()}' | while read -r key; do
echo $key
if [ $key == 61 ]; then
	xdotool key Ctrl+Shift+u
	xdotool key F5
fi
done
